import React from 'react';
import './Uploaded.css';
import { FiSearch,FiChevronDown,FiChevronLeft } from "react-icons/fi";
import ContentArea from './ContentArea';
const request = require('request');

class Uploaded extends React.Component {
  constructor(props){
    super(props)
    this.state = {attribute_data: [],selected_label:[],selected_label_attributes:{},checkbox_selected: [],display_video: false}
    this.attribute_click = this.attribute_click.bind(this)
    this.checkbox_click = this.checkbox_click.bind(this)
    this.search = this.search.bind(this)
    this.dropdown_click = this.dropdown_click.bind(this)

  }
  componentDidMount(){

    const self = this

    fetch("http://localhost:3000/attributes.json")
.then(response => response.json())
.then(data => {
  let hash = {}
  Object.keys(data[Object.keys(data)[0]][0]).map((item,index)=>{
    hash[item] = {display: false}
  })
  self.setState({
      attribute_data: data,selected_label: data[Object.keys(data)[0]][0],selected_label_attributes:hash
    })}
);




 }

  search(){
    console.log(this.state.checkbox_selected)
    this.setState({display_video:false})
    setTimeout(()=>{
      this.setState({display_video:true})
    },100)

  }
  dropdown_click(name){
    console.log(name)
    let hash = {}
    Object.keys(this.state.selected_label).map((item,index)=>{
      hash[item] = {display: false}
    })
    hash[name] = {display: true}
    console.log(hash)
    this.setState({selected_label_attributes:hash})
  }


  attribute_click(label){
    this.state.checkbox_selected.map(x => {
      this.refs[x].checked = false;
    });
    let hash = {}
    Object.keys(this.state.attribute_data[label][0]).map((item,index)=>{
      hash[item] = {display: false}
    })
    this.setState({selected_label_attributes:hash,selected_label:this.state.attribute_data[label][0],checkbox_selected: [] })
  }
  checkbox_click(c){
    console.log(c)
    const array = this.state.checkbox_selected
    this.refs[c].checked = !this.refs[c].checked
    if (this.refs[c].checked){
      array.push(c)
      this.setState({checkbox_selected:array})
    }else{

      const index = array.indexOf(c);
      if (index > -1) {
         array.splice(index, 1);
         this.setState({checkbox_selected:array})
      }
    }
  }
  render(){
      const self = this
      return (
        <div className="WorkArea2">
            <div className="SideBar">
            <button onClick={self.search}> Search </button>
                <div className="SideBarSearch">

                  <input type="text" placeholder="Search"/>
                  <span><FiSearch/></span>
                </div>
                <div>
                  <p className="ContentHead">Labels</p>
                  <div className="Labels">
                  { Object.keys(this.state.attribute_data).map(
                    function(label){
                    return <div className="LabelsContent" onClick={self.attribute_click.bind(this,label)}>
                      {label.toUpperCase()}
                    </div>
                    }
                  )

                  }
                  </div>
                </div>
                <div className="scrollCont">
                  <p className="ContentHead">Properties</p>
                  {
                    Object.keys(this.state.selected_label).map(function(obj){
                      return (<div>
                        <p className="PropertyLabel" onClick={self.dropdown_click.bind(this,obj)}>
                        {self.state.selected_label_attributes[obj].display ? <FiChevronDown / >:<FiChevronLeft/ >} {obj.toUpperCase()}</p>
                         {
                          self.state.selected_label[obj].map(function(inner_obj){
                            return(
                              <div style={{display:self.state.selected_label_attributes[obj].display ? 'flex':'none',marginTop:'2vh'}} onClick={self.checkbox_click.bind(this,obj+inner_obj)}>
                                <div className="squaredTwo">
                                  <input type="checkbox" ref={obj+inner_obj} value={obj+inner_obj} id={obj+inner_obj} name={obj+inner_obj} />
                                  <label htmlFor={obj+inner_obj}></label>
                                </div>
                                <div className="CheckBoxInput">
                                    <label> {inner_obj.toUpperCase()} </label>
                                </div>
                              </div>
                            )
                          })
                        }
                        </div>
                    )
                    })
                  }




                </div>
            </div>
            <div className="ContentArea" >
            { this.state.display_video ? <ContentArea arr={this.state.checkbox_selected} /> : ''
             }
            </div>
        </div>
      );
    }
}

export default Uploaded;
