import React from 'react';
import './WorkArea.css';
import { FiPlusCircle } from "react-icons/fi";
import Dropzone from 'react-dropzone'

class WorkArea extends React.Component{
  constructor(props){
    super(props)
    }

  render(){
  return (
    <div className="WorkArea">
        <div className="DragArea">
        <Dropzone onDrop={acceptedFiles => this.props.binded_fun()} activeStyle={{background:'green'}}>
    {({getRootProps, getInputProps}) => (
          <div {...getRootProps()} className="DragDrop">
          <input  {...getInputProps()} />
              <div >
                <span><FiPlusCircle/></span>

                  <p> Upload Your Videos Here!</p>
              </div>
              <p>(Click Or Drag-Drop your file here to start scan.)</p>
          </div>
        )}
        </Dropzone>
        </div>
    </div>
  );
}
}

export default WorkArea;
