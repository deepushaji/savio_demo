import React from 'react';
import './ContentArea.css';
import { FiPlusCircle } from "react-icons/fi";
class ContentArea extends React.Component  {
  constructor(props){
    super(props)
    this.state = {playbackRate:0.5,frameRate: 29.9,stop_time:'',segments_array:[{start_frame:'a',end_frame:'b'}]}
  }
  player = React.createRef();
      canv = React.createRef();
      frameRate = 29.9;
      ctx = '';

      componentDidMount() {
        const ar_str = this.props.arr.toString();
        fetch("http://localhost:3000/data.json")
  .then(response => response.json())
  .then(data => {
    console.log(data);
    let segments_array = []
    let segment = {start_frame:'',end_frame:''};
    let last_frame = 0;
    let start = true;
    let index = -1;
    let prev = 0;
    data.forEach((obj)=>{

      if(start){
          segment = {start_frame:obj.frame,end_frame:''}
          segments_array.push(segment)
          index = index +1;
          start = false
      }
      if((obj.frame - prev) > 30){
          segments_array[index].end_frame = prev
          segment = {start_frame:obj.frame ,end_frame:''}
          segments_array.push(segment)
          index = index +1;
      }
      prev = obj.frame
    })
  //  setTimeout(()=>{

      segments_array[segments_array.length-1].end_frame = prev
console.log(segments_array)
  //  },50)/this.state.frameRate)
    this.setState({ data: data,segments_array:segments_array });
});
           this.player.current.addEventListener('play', () => {
             //this.player.current.currentTime = this.state.data[0].frame/29.9;
               this.initiate_play()
           })
           this.ctx = this.canv.current.getContext('2d');
           this.player.current.playbackRate = 0.5;

           this.ctx.canvas.width = 320;
           this.ctx.canvas.height = 180;
           this.playVid = this.playVid.bind(this)
           this.fastVid = this.fastVid.bind(this)
           this.slowVid = this.slowVid.bind(this)
           this.pauseVid = this.pauseVid.bind(this)
           this.resetVid = this.resetVid.bind(this)
       }
       playVid() {
    this.player.current.play();

}
slowVid(){
  console.log(this.player.current.playbackRate)
  if(this.player.current.playbackRate > 0.11){
  this.player.current.playbackRate = this.player.current.playbackRate - 0.1
  this.setState({playbackRate:this.player.current.playbackRate})
}
}
pauseVid(){
  this.player.current.pause();
}
fastVid(){
  if(this.player.current.playbackRate < 1.99){
    this.player.current.playbackRate = this.player.current.playbackRate + 0.1
    this.setState({playbackRate:this.player.current.playbackRate})
  }
}
goToVideo(start,end){
  //this.player.current.pause();
  //this.player.current.play();
  if((end-start) < 2){
    end = start + 2
  }
  this.player.current.currentTime = start;
  setTimeout(()=>{
    this.player.current.play()
  },100)
  //this.player.current.load();

  this.player.current.playbackRate = 0.5;
  this.setState({playbackRate:this.player.current.playbackRate, stop_time:end})
}
resetVid(){
  this.player.current.pause();
this.player.current.currentTime = 0;
this.player.current.load();
this.player.current.playbackRate = 0.5;
this.setState({playbackRate:this.player.current.playbackRate})
}

       draw_rectangle(curFrame){
         this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
         const object_array = this.state.data.filter(data => data.frame === curFrame)
         if(object_array.length !== 0){
             object_array[0].objects.forEach(box => {

               var x1 = Math.floor((box.x1/1280)*this.ctx.canvas.width);
               var y1 = Math.floor((box.x2/720)*this.ctx.canvas.height);
               var w = Math.floor(((box.x3 - box.x1)/1280)*this.ctx.canvas.width);
               var h = Math.floor(((box.x4 - box.x2)/720)*this.ctx.canvas.height);
               //console.log(x1, y1, x2, y2)
               this.ctx.strokeStyle = 'red';
                this.ctx.strokeRect(x1, y1, w, h);
            });
        }
        //  this.ctx.strokeRect(v1, v2, v3, v4);
       }

       initiate_play(){
         //console.log(this.state.data)
         const curTime = this.player.current.currentTime

         const curFrame = Math.floor(curTime * 29.9);
          //console.log(curTime,curFrame)
          if(parseInt(this.state.stop_time) == parseInt(this.player.current.currentTime)){
            this.pauseVid()
          }
         this.draw_rectangle(curFrame)
         if (true)
                setTimeout(this.initiate_play.bind(this), 50);
       }
  render(){
  return (
    <div className="Content">
      <div>
        <h3>1 Video Found!</h3>
        <button>Click Here To View!</button>
      </div>

      <div className="Modal">
        <div className="ModalMain">
        <div className="controls">
        <span>SEGMENTS :</span>
        {  this.state.segments_array.map((obj,item)=>{

              return (<button onClick={()=>{this.goToVideo(obj.start_frame/this.state.frameRate, obj.end_frame/this.state.frameRate)}} >
              {parseInt(obj.start_frame/this.state.frameRate) +'s - '+parseInt(obj.end_frame/this.state.frameRate)+'s'}
              </button>)
            })
        }
        </div>
            <div className="ModalContent">
                  <video muted ref={this.player} id="video" src="crowd.mp4"></video>
                  <canvas ref={this.canv} id="canvas"></canvas>
            </div>
            <div className="controls">
            <span>Speed: x{this.state.playbackRate.toFixed(1)}</span>
            <button onClick={this.playVid}>PLAY</button>
            <button onClick={this.pauseVid}>PAUSE</button>
            <button onClick={this.slowVid}> {'SLOW <<'} </button>
            <button onClick={this.fastVid}> {'FAST >>'} </button>
            <button onClick={this.resetVid}> RESET </button>
            </div>
        </div>

      </div>
    </div>
  );
}
}

export default ContentArea;
