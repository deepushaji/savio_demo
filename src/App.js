import React from 'react';
import './App.css';
import Uploaded from './Uploaded';
import WorkArea from './WorkArea';
import { FiLogOut } from "react-icons/fi";

class App extends React.Component{
  constructor(props){
    super(props)
    this.change_state = this.change_state.bind(this)
    this.state =  {display: <WorkArea binded_fun={this.change_state}/>}
    }
  change_state(){
    this.setState({display: <Uploaded />})
  }
  render(){
  return (
    <div className="App">
      <header className="App-header">
        <div className="heading">EyesAge</div>
        <div className="tab">
          <span>Upload</span>
          </div>
        <div className="user">ashoka.deepu@gmail.com</div>

        <div className="dropdown" style={{display:'none'}}>
          <p><FiLogOut /> Log Out</p>
        </div>

      </header>
      {this.state.display}
    </div>
  );
}
}

export default App;
